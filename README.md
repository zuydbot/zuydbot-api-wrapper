# Zuydbot API Wrapper
[![PyPi Version](https://img.shields.io/pypi/v/zuydbot-api.svg)](https://pypi.org/project/zuydbot-api/)

The Zuydbot API Wrapper can be used in python with the Zuydbot API. Zuydbot gathers your deadlines from Moodle and your lessons from Untis in one place (Discord). The API allows you to easily use deadlines and lessons in your own projects.

More info: [Zuydbot website](https://zuydbot.cc)

## Installation
Installation is easy! Register on the [dashboard](https://app.zuydbot.cc/dashboard) to get an API key.
More info on getting your API key can be found on our [setup page](https://zuydbot.cc/setup.html).

Once you've got your API key you can install the latest wrapper with **pip**.
```sh
pip install zuydbot-api
```

After that, getting started is easy!

## Usage
Getting started is simple. Just define the API client and get your deadlines.
```python
from zuydbot_api import APIConnection

client = APIConnection("API_KEY")
client.get_deadlines() 
client.get_lessons()

deadlines = client.deadlines
lessons = client.lessons
```
The *get()* functions will send a request to the api for your latest deadlines and lessons, and this data will then be stored as an object in a list. So you only need to get them once, and can freely use them after that.

## Support
If you need any help using the wrapper, the API or Zuydbot, you can always join us on our [Discord server](https://discord.gg/f8hbnqf).
