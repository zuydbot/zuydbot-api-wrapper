from setuptools import setup

setup(name='zuydbot_api',
      version='0.1',
      description='API Wrapper for Zuydbot',
      url='https://gitlab.com/zuydbot/zuydbot-api-wrapper',
      author='Alain Lardinois',
      author_email='alain@ip-ict.nl',
      license='',
      packages=['zuydbot_api'])
